#include <iostream>
#include <string>

using namespace std;

class Animal
{

public:
	virtual void Voice()
	{
		cout << "text" << endl;
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		cout << " C����� ���� \t - ���" << endl;
	}

};
class Cat : public Animal
{
public:
	void Voice() override
	{
		cout << "����� ������� \f - ���" << endl;
	}
};
class Bird : public Animal
{
public:
	void Voice() override
	{
		cout << "������ ������ \t - �����" << endl;
	}
};
int main()
{
	setlocale(LC_ALL, "ru");

	cout << "�������� ���������� :  " << endl << endl;

	Dog bark;
	bark.Voice();

	Cat soften;
	soften.Voice();

	Bird tweet;
	tweet.Voice();

	cout << " ���������� ������ � ������� : " << endl << endl;

	int size = 3;

	Animal* arr = new Animal[size];
	{
		arr[0] = &bark;
		arr[1] = &soften;
		arr[2] = &tweet;

		for (int i = 0; i < size; i++)
		{
			arr[i].Voice();
		}
		delete[] arr;
	};
	return 0;
};